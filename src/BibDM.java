import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      if(liste.size()==0){
        return null;
      }
      Integer plusPetit = liste.get(0);
      for(Integer i:liste){
        if(i<plusPetit){
          plusPetit=i;
        }
      }
      return plusPetit;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeur donnée
     * @param valeur
     * @param liste
     * @return true si tous les élements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T i:liste){
          if(valeur.compareTo(i)>-1){
            return false;
          }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        ArrayList<T> res = new ArrayList<T>();
        for(T elem:liste1){
            if(liste2.contains(elem) && !res.contains(elem)){
              res.add(elem);
            }
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        ArrayList<String> res = new ArrayList<String>();
        if(texte.length()!=0){
            for(String s : texte.split(" ")){
                if(s.length()!=0){
                    res.add(s);
                }
            }
        }
        return res;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

        if(texte.length()==0){
            return null;
        }

        List<String> listeMots = decoupe(texte);
        Collections.sort(listeMots);
        List<String> res = new ArrayList<String>();
        List<Integer> nombreMots = new ArrayList<Integer>();

        for(String mot: listeMots){
            if(res.contains(mot)){
              nombreMots.set(res.indexOf(mot), nombreMots.get(res.indexOf(mot)) + 1);
            }else{
              res.add(mot);
              nombreMots.add(1);
            }
        }

        Integer max = nombreMots.get(0);
        for(Integer n:nombreMots){
            if(n>max){
              max = n;
            }
        }
        return res.get(nombreMots.indexOf(max));
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int count = 0;
        if(chaine.length()<=1){
          return false;
        }
        for(int i=0; i<chaine.length(); i++){
          if(chaine.charAt(i)=='(')
              count++;
          if(chaine.charAt(i)==')')
              count--;
        }
        if(count==0){
          return true;
        }
        return false;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        char sFin= chaine.charAt(chaine.length()-1);
        char sDeb= chaine.charAt(0);

        int countP=0;
        int countC=0;

        if((chaine.length()<=1) || (sDeb=='(' && sFin!=')') || (sDeb=='[' && sFin!=']')){
          return false;
        }
        for(int i=0; i<chaine.length(); i++){
          if(chaine.charAt(i)=='(')
              countP++;
          if(chaine.charAt(i)==')')
              countP--;
          if(chaine.charAt(i)=='[')
              countC++;
          if(chaine.charAt(i)==']')
              countC--;
        }
        if(countC==0 && countP==0){
          return true;
        }
        return false;
    }


    /**
     * Recherche par dichotomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      return false;
    }

}
